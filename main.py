#Importação das bibliotecas
import io
import os 
from google.cloud import vision
from google.cloud.vision import types 
from PIL import Image, ImageDraw, ImageFont

#Listas e Variaveis Utilizadas
listOfObjects = []
listOfVerticesX1 = []
listOfVerticesY1 = []
objectNumber = 1 

client = vision.ImageAnnotatorClient()

#Leitura da imagem
file_name = os.path.join(os.path.dirname(__file__),"garrafas.jpg")

with open(file_name, 'rb') as image_file:
        content = image_file.read()

image = vision.types.Image(content=content)

#Utilização da API object_location para o retorno das informações sobre a imagem enviada a API.
objects = client.object_localization(
    image=image).localized_object_annotations

#Salvando o nome dos objetos localizados em uma lista, e também os vertices de cada objeto.
for object_ in objects:
    if(object_.score >= 0.7):
        ObjectName = object_.name + " " + str(objectNumber)
        listOfObjects.append(ObjectName)
        objectNumber += 1
        for vertex in object_.bounding_poly.normalized_vertices:
            listOfVerticesX1.append(vertex.x)
            listOfVerticesY1.append(vertex.y)

im = Image.open(file_name)
width, height = im.size
font = ImageFont.truetype("arial.ttf",36)
draw = ImageDraw.Draw(im)

#Identificando os objetos de acordo com os vertices encontrados
for i in range(0,len(listOfVerticesX1)-3,4):
    draw.polygon([(listOfVerticesX1[i]*width,listOfVerticesY1[i]*height), (listOfVerticesX1[i+1]*width,listOfVerticesY1[i+1]*height), (listOfVerticesX1[i+2]*width,listOfVerticesY1[i+2]*height), (listOfVerticesX1[i+3]*width,listOfVerticesY1[i+3]*height)], 
                fill=None,outline=(124,252,0))    

#Inserindo o nome dos objetos identificados
posicao = 0
for i in range (0,len(listOfObjects)):
    draw.text((listOfVerticesX1[i+posicao]*width,listOfVerticesY1[i+posicao]*height),listOfObjects[i],fill="black",font=font)
    posicao +=3
del draw

#Mostrando a imagem ao usuário
im.show()
